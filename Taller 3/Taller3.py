import datetime
import sys

def promedios_ventana (entrada, n):
    
    datos_entrada=[] #lista de listas de la entrada en str
    matrix=[] #lista de listas de la entrada en float
    promedios=[] #salida promedios
   
    for linea in entrada:
        datos_entrada.append (linea.split (',')) #divide los datos de cada fila de la entrada con comas

    for i in range(len(datos_entrada)):
        matrix.append([]) #appendea una lista vacía por cada fila de la entrada
        matrix[i].append(datetime.datetime.strptime(datos_entrada[i][0], '%Y-%m-%dT%H:%M:%S')) #appendea el tiempo en formato datetime en la primer columna de cada fila
        #transformo los elementos de la entrada distintos de 'NA' a float
        for e in (datos_entrada[i][1:]): 
            if e == 'NA' or e == 'NA\n': 
                matrix[i].append('NA')
            else:
                matrix[i].append (float(e))

    w= len(matrix) #cantidad de mediciones
    for i in range (0, w-n+1): #rango cantidad de filas de la matriz de salida segun el tamaño de ventana n
        promedios.append([]) #appendea una lista vacía por cada fila de la matriz de salida
        promedios[i].append('{:.2f}'.format((matrix[i+n-1][0]-matrix[i][0]).total_seconds())) #appendea el rango de tiempo de la ventana en la primer columna de cada fila

        for k in range (1, len(matrix[i])): #len(matrix[i]) cantidad de columnas
            suma_ventana= 0
            for j in range (i, i+n): #tamaño de la ventana
                if matrix[j][k] == 'NA':
                    suma_ventana = 'NA' #alguno de los valores dentro de la ventana es 'NA' la suma = 'NA'
                    break
                else:
                    suma_ventana = suma_ventana + matrix [j][k] #suma temperaturas de la ventana
                    
            if suma_ventana == 'NA':
                promedios[i].append ('NA') #suma = 'NA' el promedio de la ventana es 'NA'
            else:
                promedios[i].append ('{:.2f}'.format(suma_ventana/n)) #promedio de la ventana con dos cifras significativas
    return promedios

n = int(sys.argv[3]) 
entrada = open(sys.argv[1], 'r')
promedios_final = promedios_ventana(entrada, n)
salida = open(sys.argv[2], 'w')
for i in range (len(promedios_final)):
    print (','.join(promedios_final[i]), file = salida)

salida.close()
entrada.close()