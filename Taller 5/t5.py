import numpy as np
import time
import random
import sys

global N
N = 1000

def matrizCeros():
    matriz = []
    for i in range (0, N):
        matriz.append ([]) #genera una lista con N listas vacias adentro
        for j in range (0, N):
            matriz[i].append (0.0) #dentro de cada lista vacía agrega N veces 0.0
    return matriz #devuelve una matriz NxN de ceros

def matrizAleatoria(a):
    for i in range (0, N):
        for j in range (0, N):
            a[i][j] = random.random() #cambia cada posición de la matriz 'a' por un número aleatorio
    return a
     
def multMatrices(a, b, res):
    if isinstance(a, np.matrix): #si 'a' es una matriz de Numpy realiza la multiplicación usando el modulo Numpy
        mult_matrices = np.matmul(a, b)
        np.copyto(res, mult_matrices)
    else: #si las matrices son listas de listas realiza la siguiente operación
        for i in range (0, N):
            for j in range (0, N):
                for k in range (0, N):
                    res[i][j]+= a[i][k]*b[k][j]  #multiplica la matriz a con i filas k columnas por la matriz b con k filas j columnas y devuelve la matriz res con i filas j columnas
    return res 

def medirTiempos(fn, *args):
    tiempo_inicial = time.time() #tiempo antes de ejecutar la función
    print (fn (*args)) #ejecuta función
    tiempo_final = time.time() #tiempo después de ejecutar la función
    tiempo_actual = tiempo_final - tiempo_inicial #diferencia entre el tiempo final e inicial
    return tiempo_actual

def realizarExperimento():
    #genera tres matrices de ceros
    A = matrizCeros()
    B = matrizCeros()
    C = matrizCeros()
    #completa matrices A y B con números aleatorios
    matrizAleatoria(A)
    matrizAleatoria(B)
    
    print ('Tiempo total listas: ', medirTiempos(multMatrices, A, B, C))
    #equivalente de las matrices A, B y C en NumPy  
    A = np.matrix(matrizAleatoria(A))
    B = np.matrix(matrizAleatoria(B))
    C = np.matrix(matrizCeros())
    
    print ('Tiempo total numpy: ', medirTiempos(multMatrices, A, B, C))

realizarExperimento()

if __name__ == '__main__':
    if len(sys.argv) > 1:
        N = int(sys.argv[1])
    realizarExperimento()
