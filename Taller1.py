﻿def Hazard(n):
    i=1
    suma=0
#contador
    while i<=n:
        r= ((((-1)**(i+1))*2)/(2*i-1))
        suma=suma+r
        #agrega a la suma el resultado de la funcion mientras i<=n
        
        i=i+1
    return suma

def esPrimo(x):
    i=1
    suma=0
	#contador
    while i<=x:
        if x%i==0:
            suma=suma+1
		#cada vez que el resto de la división es 0 se suma 1 al contador
        i=i+1
    r= (suma==2)
    #Si x es divisible solo por 1 y si mismo la suma va a ser 2 y la función devuelve como resultado True
    return r

def Lukaku (n):
	suma=0
	#contador
	exp=2
	while suma<n:
		if esPrimo(2**exp-1):
			suma=suma+1
			res= (2**exp-1)
			#Si res es primo se suma un 1 al contador
		exp=exp+1
	return res