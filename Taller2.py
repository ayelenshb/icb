#Ejercicio1
def existEnL2 (x, l2):
    for i in range (0, len(l2)):
        if l2[i]==x:
            return True
#Devuelve verdadero si existe un x tal que coincide con el i-ésimo elemento de la lista L2

def cantAparicionesSub (l1, l2):
    suma=0
#contador
    for i in range (0, len(l1)):
        if existEnL2 (l1[i], l2):
            suma=suma+1
    return suma
#Si el i-ésimo elemento de la lista l1 coincide con el i-ésimo elemento de la lista l2 se suma 1 al contador.

#Ejercicio2
def posicionMax (l):
    for i in range (0, len(l)):
        if l[i]==max(l):
            return i
#Me devuelve la posición del valor máximo de la lista l

def listaTriangular (l):
    suma=0
#contador
    for i in range (0, posicionMax(l)):
        if l[i]<l[i+1]:
            suma=suma+1
#Evalúa si la lista es creciente entre la posición 0 y la posición del valor máximo de la misma. Suma 1 al contador por cada l[i]<l[i+1].
    for i in range (posicionMax(l), (len(l)-1)):
        if l[i]>l[i+1]:
            suma=suma+1
#Evalua si la lista es decreciente entre la posición del valor máximo y la anteúltima posición de la lista. Se evalua hasta len(l)-1 ya que para la última posición l[i+1] queda fuera de rango. Suma 1 al contador por cada l[i]>l[i+1].
    r= (suma==len(l)-1)
#Si la lista crece a partir de más de una posición la suma será distinta al largo de la lista-1. Le resto 1 al largo de la lista ya que evalúo hasta la anteúltima posición.
    return r

        
